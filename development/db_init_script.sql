-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Creato il: Apr 04, 2021 alle 12:40
-- Versione del server: 5.6.49
-- Versione PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `economy`
--
CREATE DATABASE IF NOT EXISTS `economy` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `economy`;

-- --------------------------------------------------------

--
-- Struttura della tabella `jobs`
--

CREATE TABLE `jobs` (
                        `id` int(10) UNSIGNED NOT NULL,
                        `user_id` int(10) UNSIGNED DEFAULT NULL,
                        `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
                        `departure` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `arrival` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `limitations` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                        `required_rank_id` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `jobs`
--

INSERT INTO `jobs` (`id`, `user_id`, `title`, `description`, `departure`, `arrival`, `category`, `limitations`, `required_rank_id`) VALUES
(1, 1, 'Consequatur maiores dolores.', 'Corrupti exercitationem dolor maxime cupiditate nostrum soluta sint est autem ducimus error.', 'qilp', 'zzdd', 'Voluptas in voluptatem natus est.', 'Quia quae ex voluptatem magni ipsum.', 1),
(2, 1, 'Facere dolore iure.', 'Natus voluptatibus eos vel rem et doloribus id.', 'poru', 'npie', 'Consequatur architecto rerum doloribus neque.', 'Molestiae cum in soluta.', 3),
(3, 3, 'Ducimus dignissimos labore.', 'Voluptatem a dolorem non placeat numquam eligendi soluta quos veritatis odio aperiam tempore aut.', 'ivdr', 'qubt', 'Commodi reiciendis laudantium animi repellat facilis consequuntur.', 'Cumque qui autem in vel et id et.', 3),
(4, 1, 'Et consectetur delectus.', 'Et rem eum aliquam vero aut officia non.', 'wsrc', 'kuai', 'A officiis rerum qui sint sunt.', 'Dignissimos ut nulla nihil mollitia labore.', 4),
(5, 1, 'Sed accusantium.', 'Sunt laboriosam aliquid voluptatem soluta ut doloribus accusantium explicabo quo id.', 'rhos', 'dzra', 'Ullam dolorum facilis tenetur natus non.', 'Quae et enim ad dolorem deleniti quis.', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `job_license`
--

CREATE TABLE `job_license` (
                               `job_id` int(10) UNSIGNED NOT NULL,
                               `license_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `job_license`
--

INSERT INTO `job_license` (`job_id`, `license_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(1, 2),
(5, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `licenses`
--

CREATE TABLE `licenses` (
                            `id` int(10) UNSIGNED NOT NULL,
                            `license_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `licenses`
--

INSERT INTO `licenses` (`id`, `license_name`) VALUES
(1, 'IFR'),
(2, 'VFR');

-- --------------------------------------------------------

--
-- Struttura della tabella `license_user`
--

CREATE TABLE `license_user` (
                                `user_id` int(10) UNSIGNED NOT NULL,
                                `license_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `license_user`
--

INSERT INTO `license_user` (`user_id`, `license_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(1, 2),
(3, 2),
(5, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `migrations`
--

CREATE TABLE `migrations` (
                              `id` int(10) UNSIGNED NOT NULL,
                              `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                              `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(2, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(3, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(4, '2016_06_01_000004_create_oauth_clients_table', 1),
(5, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(6, '2014_10_12_000000_create_users_table', 2),
(7, '2014_10_12_100000_create_password_resets_table', 2),
(8, '2018_11_03_162532_create_jobs_table', 2),
(9, '2018_11_04_134106_create_ranks_table', 2),
(10, '2018_11_04_140558_alter_user_table_add_rank_foreign_key', 2),
(11, '2018_11_04_141200_create_licenses_table', 2),
(12, '2018_11_04_145031_create_license_user_table', 2),
(13, '2018_11_04_151558_alter_jobs_table_add_rank_foreign_key', 2),
(14, '2018_11_04_151943_create_job_license_table', 2),
(15, '2019_04_25_100147_drop_column_password_from_users', 2),
(16, '2020_04_11_182400_create_roles_table', 2),
(17, '2020_04_11_182634_alter_user_table_add_role_foreign_key', 2),
(18, '2020_04_25_130337_drop_passowd_reset_table', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
                                       `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `user_id` int(11) DEFAULT NULL,
                                       `client_id` int(11) NOT NULL,
                                       `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                       `scopes` text COLLATE utf8mb4_unicode_ci,
                                       `revoked` tinyint(1) NOT NULL,
                                       `created_at` timestamp NULL DEFAULT NULL,
                                       `updated_at` timestamp NULL DEFAULT NULL,
                                       `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
                                    `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                    `user_id` int(11) NOT NULL,
                                    `client_id` int(11) NOT NULL,
                                    `scopes` text COLLATE utf8mb4_unicode_ci,
                                    `revoked` tinyint(1) NOT NULL,
                                    `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_clients`
--

CREATE TABLE `oauth_clients` (
                                 `id` int(10) UNSIGNED NOT NULL,
                                 `user_id` int(11) DEFAULT NULL,
                                 `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
                                 `personal_access_client` tinyint(1) NOT NULL,
                                 `password_client` tinyint(1) NOT NULL,
                                 `revoked` tinyint(1) NOT NULL,
                                 `created_at` timestamp NULL DEFAULT NULL,
                                 `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, 1, 'Postman', 'YjyoSfV2g95j8LAWuOwXI5CM8uSsNmR3lwB2zMcl', 'https://oauth.pstmn.io/v1/callback', 0, 0, 0, '2021-04-02 17:43:04', '2021-04-02 17:43:04');

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
                                                 `id` int(10) UNSIGNED NOT NULL,
                                                 `client_id` int(11) NOT NULL,
                                                 `created_at` timestamp NULL DEFAULT NULL,
                                                 `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
                                        `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                                        `revoked` tinyint(1) NOT NULL,
                                        `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `ranks`
--

CREATE TABLE `ranks` (
                         `id` int(10) UNSIGNED NOT NULL,
                         `rank_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `ranks`
--

INSERT INTO `ranks` (`id`, `rank_name`) VALUES
(1, 'Novice'),
(2, 'Medium'),
(3, 'Senior'),
(4, 'Expert');

-- --------------------------------------------------------

--
-- Struttura della tabella `roles`
--

CREATE TABLE `roles` (
                         `id` int(10) UNSIGNED NOT NULL,
                         `role_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `roles`
--

INSERT INTO `roles` (`id`, `role_name`) VALUES
(1, 'User'),
(2, 'Administrator');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
                         `id` int(10) UNSIGNED NOT NULL,
                         `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `pilot_callsign` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `rank_id` int(10) UNSIGNED DEFAULT NULL,
                         `role_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `username`, `pilot_callsign`, `rank_id`, `role_id`) VALUES
(1, 'user_0', 'SCI045', 4, 1),
(2, 'user_1', 'SCI352', 4, 1),
(3, 'user_2', 'SCI564', 1, 1),
(4, 'user_3', 'SCI169', 3, 1),
(5, 'user_4', 'SCI514', 2, 1);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `jobs`
--
ALTER TABLE `jobs`
    ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_user_id_foreign` (`user_id`),
  ADD KEY `jobs_required_rank_id_foreign` (`required_rank_id`);

--
-- Indici per le tabelle `job_license`
--
ALTER TABLE `job_license`
    ADD PRIMARY KEY (`job_id`,`license_id`),
  ADD KEY `job_license_license_id_foreign` (`license_id`);

--
-- Indici per le tabelle `licenses`
--
ALTER TABLE `licenses`
    ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `license_user`
--
ALTER TABLE `license_user`
    ADD PRIMARY KEY (`user_id`,`license_id`),
  ADD KEY `license_user_license_id_foreign` (`license_id`);

--
-- Indici per le tabelle `migrations`
--
ALTER TABLE `migrations`
    ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
    ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indici per le tabelle `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
    ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `oauth_clients`
--
ALTER TABLE `oauth_clients`
    ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indici per le tabelle `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
    ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indici per le tabelle `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
    ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indici per le tabelle `ranks`
--
ALTER TABLE `ranks`
    ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `roles`
--
ALTER TABLE `roles`
    ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD KEY `users_rank_id_foreign` (`rank_id`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `jobs`
--
ALTER TABLE `jobs`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `licenses`
--
ALTER TABLE `licenses`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `migrations`
--
ALTER TABLE `migrations`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT per la tabella `oauth_clients`
--
ALTER TABLE `oauth_clients`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `ranks`
--
ALTER TABLE `ranks`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `roles`
--
ALTER TABLE `roles`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `jobs`
--
ALTER TABLE `jobs`
    ADD CONSTRAINT `jobs_required_rank_id_foreign` FOREIGN KEY (`required_rank_id`) REFERENCES `ranks` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `jobs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Limiti per la tabella `job_license`
--
ALTER TABLE `job_license`
    ADD CONSTRAINT `job_license_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `job_license_license_id_foreign` FOREIGN KEY (`license_id`) REFERENCES `licenses` (`id`) ON DELETE CASCADE;

--
-- Limiti per la tabella `license_user`
--
ALTER TABLE `license_user`
    ADD CONSTRAINT `license_user_license_id_foreign` FOREIGN KEY (`license_id`) REFERENCES `licenses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `license_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Limiti per la tabella `users`
--
ALTER TABLE `users`
    ADD CONSTRAINT `users_rank_id_foreign` FOREIGN KEY (`rank_id`) REFERENCES `ranks` (`id`),
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
--
-- Database: `php_bb`
--
CREATE DATABASE IF NOT EXISTS `php_bb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `php_bb`;

-- --------------------------------------------------------

--
-- Struttura della tabella `phpbb_profile_fields_data`
--

CREATE TABLE `phpbb_profile_fields_data` (
                                             `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
                                             `pf_phpbb_interests` mediumtext COLLATE utf8_bin NOT NULL,
                                             `pf_phpbb_occupation` mediumtext COLLATE utf8_bin NOT NULL,
                                             `pf_phpbb_location` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                                             `pf_phpbb_youtube` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                                             `pf_phpbb_skype` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                                             `pf_phpbb_icq` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                                             `pf_phpbb_facebook` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                                             `pf_phpbb_googleplus` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                                             `pf_phpbb_website` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                                             `pf_phpbb_twitter` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                                             `pf_phpbb_yahoo` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                                             `pf_phpbb_aol` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                                             `pf_callsign` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                             `pf_vid` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `phpbb_profile_fields_data`
--

INSERT INTO `phpbb_profile_fields_data` (`user_id`, `pf_phpbb_interests`, `pf_phpbb_occupation`, `pf_phpbb_location`, `pf_phpbb_youtube`, `pf_phpbb_skype`, `pf_phpbb_icq`, `pf_phpbb_facebook`, `pf_phpbb_googleplus`, `pf_phpbb_website`, `pf_phpbb_twitter`, `pf_phpbb_yahoo`, `pf_phpbb_aol`, `pf_callsign`, `pf_vid`) VALUES
(1, '', '', '', '', '', '', '', '', '', '', '', '', 'SCI001', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `phpbb_users`
--

CREATE TABLE `phpbb_users` (
                               `user_id` int(10) UNSIGNED NOT NULL,
                               `user_type` tinyint(4) NOT NULL DEFAULT '0',
                               `group_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '3',
                               `user_permissions` mediumtext COLLATE utf8_bin NOT NULL,
                               `user_perm_from` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
                               `user_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_regdate` int(10) UNSIGNED NOT NULL DEFAULT '0',
                               `username` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `username_clean` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_password` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_passchg` int(10) UNSIGNED NOT NULL DEFAULT '0',
                               `user_email` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_email_hash` bigint(20) NOT NULL DEFAULT '0',
                               `user_birthday` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_lastvisit` int(10) UNSIGNED NOT NULL DEFAULT '0',
                               `user_lastmark` int(10) UNSIGNED NOT NULL DEFAULT '0',
                               `user_lastpost_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
                               `user_lastpage` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_last_confirm_key` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_last_search` int(10) UNSIGNED NOT NULL DEFAULT '0',
                               `user_warnings` tinyint(4) NOT NULL DEFAULT '0',
                               `user_last_warning` int(10) UNSIGNED NOT NULL DEFAULT '0',
                               `user_login_attempts` tinyint(4) NOT NULL DEFAULT '0',
                               `user_inactive_reason` tinyint(4) NOT NULL DEFAULT '0',
                               `user_inactive_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
                               `user_posts` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
                               `user_lang` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_timezone` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_dateformat` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT 'd M Y H:i',
                               `user_style` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
                               `user_rank` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
                               `user_colour` varchar(6) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_new_privmsg` int(11) NOT NULL DEFAULT '0',
                               `user_unread_privmsg` int(11) NOT NULL DEFAULT '0',
                               `user_last_privmsg` int(10) UNSIGNED NOT NULL DEFAULT '0',
                               `user_message_rules` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
                               `user_full_folder` int(11) NOT NULL DEFAULT '-3',
                               `user_emailtime` int(10) UNSIGNED NOT NULL DEFAULT '0',
                               `user_topic_show_days` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
                               `user_topic_sortby_type` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT 't',
                               `user_topic_sortby_dir` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT 'd',
                               `user_post_show_days` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
                               `user_post_sortby_type` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT 't',
                               `user_post_sortby_dir` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT 'a',
                               `user_notify` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
                               `user_notify_pm` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
                               `user_notify_type` tinyint(4) NOT NULL DEFAULT '0',
                               `user_allow_pm` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
                               `user_allow_viewonline` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
                               `user_allow_viewemail` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
                               `user_allow_massemail` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
                               `user_options` int(10) UNSIGNED NOT NULL DEFAULT '230271',
                               `user_avatar` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_avatar_type` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_avatar_width` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
                               `user_avatar_height` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
                               `user_sig` mediumtext COLLATE utf8_bin NOT NULL,
                               `user_sig_bbcode_uid` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_sig_bbcode_bitfield` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_jabber` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_actkey` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_newpasswd` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_form_salt` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
                               `user_new` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
                               `user_reminded` tinyint(4) NOT NULL DEFAULT '0',
                               `user_reminded_time` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `phpbb_users`
--

INSERT INTO `phpbb_users` (`user_id`, `user_type`, `group_id`, `user_permissions`, `user_perm_from`, `user_ip`, `user_regdate`, `username`, `username_clean`, `user_password`, `user_passchg`, `user_email`, `user_email_hash`, `user_birthday`, `user_lastvisit`, `user_lastmark`, `user_lastpost_time`, `user_lastpage`, `user_last_confirm_key`, `user_last_search`, `user_warnings`, `user_last_warning`, `user_login_attempts`, `user_inactive_reason`, `user_inactive_time`, `user_posts`, `user_lang`, `user_timezone`, `user_dateformat`, `user_style`, `user_rank`, `user_colour`, `user_new_privmsg`, `user_unread_privmsg`, `user_last_privmsg`, `user_message_rules`, `user_full_folder`, `user_emailtime`, `user_topic_show_days`, `user_topic_sortby_type`, `user_topic_sortby_dir`, `user_post_show_days`, `user_post_sortby_type`, `user_post_sortby_dir`, `user_notify`, `user_notify_pm`, `user_notify_type`, `user_allow_pm`, `user_allow_viewonline`, `user_allow_viewemail`, `user_allow_massemail`, `user_options`, `user_avatar`, `user_avatar_type`, `user_avatar_width`, `user_avatar_height`, `user_sig`, `user_sig_bbcode_uid`, `user_sig_bbcode_bitfield`, `user_jabber`, `user_actkey`, `user_newpasswd`, `user_form_salt`, `user_new`, `user_reminded`, `user_reminded_time`) VALUES
(1, 0, 3, '', 0, '', 0, 'testuser', '', '$2y$10$vF8QZwH6YkmPHzvDC5qT/.EZYYSsolDzUsdmBcxzCbVHdoy.KLxga', 0, '', 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, '', '', 'd M Y H:i', 0, 0, '', 0, 0, 0, 0, -3, 0, 0, 't', 'd', 0, 't', 'a', 0, 1, 0, 1, 1, 1, 1, 230271, '', '', 0, 0, '', '', '', '', '', '', '', 1, 0, 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `phpbb_profile_fields_data`
--
ALTER TABLE `phpbb_profile_fields_data`
    ADD PRIMARY KEY (`user_id`);

--
-- Indici per le tabelle `phpbb_users`
--
ALTER TABLE `phpbb_users`
    ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username_clean` (`username_clean`),
  ADD KEY `user_birthday` (`user_birthday`),
  ADD KEY `user_email_hash` (`user_email_hash`),
  ADD KEY `user_type` (`user_type`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `phpbb_users`
--
ALTER TABLE `phpbb_users`
    MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
