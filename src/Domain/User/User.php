<?php

class User {
    // Properties
    public $name;
    public $password;

    function __construct($name, $password) {
        $this->name = $name;
        $this->password = $password;
    }
    
    // Methods
    function set_name($name) {
        $this->name = $name;
    }
    function get_name() {
        return $this->name;
    }

    function get_password() {
        return $this->password;
    }  
}